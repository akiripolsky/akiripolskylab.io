# On rapid iterative testing

Recently a following question popped up and I am sharing my answer. This will hopefully serve as a basis for a future blog post.

---

> Has anyone done any rapid iterative prototype testing? Any advice or experiences to share?

Previously I worked (for a month) with a team that iterated very quickly – my goal was just to find as many usability issues as possible with as low effort as possible so they could improve the design and repeat the cycle frequently. I enjoyed that work and I learned a lot (I think it is clear from the length of my comment 😊). Here are my recommendations:

#### Make it clear that the outcomes are qualitative, not quantitative

It’s better not to collect information that stakeholders might try to interpret quantitatively (e.g. success rate) and correct them if they make wrong conclusions. 

#### Do usability inspection, rather than usability testing if possible

Heuristic review, cognitive walkthrough or expert review can produce a ton of valuable feedback especially in the early iterations and are less time-consuming. My favorite is the expert review because its results are the most valuable from all the other inspection methods and the experts are super easy to recruit – in our case, there are at least 62 experts in our company. 😄 I can share more details if it is interesting.

#### If you are doing usability testing, try to save time on everything you can

For example:

- Don’t produce any documentation that is not necessary.
- Save time on recruiting by talking to internal folks (newcomers are awesome).
- I tested usually with only 2 participants because the maturity of the designs was extremely low (we discovered catastrophic usability issues with nearly every participant). In the article ‘[Guerrilla HCI](ftp://ftp.cs.umanitoba.ca/pub/cs371/Readings/Guerrilla.pdf)’, Nielsen mentions a case where he ‘tested with just a single user’ per iteration. Btw the article is great. 🤩
- Have a good note-taker that can spot usability issues. Spend time on debrief with the note-taker (+ the rest of the team ideally) where you sum up what happened. It can save you time spent on analysis later.
- Report findings as close to the “designers’ hands” as possible. I reported directly to InVision as comments (in our case it would be Figma). 

It’s worth it to spend more time only on stuff that you will be reusing in all the iterations – e.g. scenarios.

#### If you decide to save time on usability testing, make sure you spend a similarly small amount of time on each of the steps of the research process 

Spending a lot of time on preparation, execution, or analysis and then not reporting the findings properly is a waste of material and degradation of your work. On the other hand, saving time on preparation, execution, or analysis and then doing a huge and shiny report/insights can mislead the stakeholders to think that you can do normal study in such a short time and they might have incorrect expectations in the future. 

#### Keep up with the changes

The designs can change way too often and it’s extremely annoying to learn that you tested something that is not up to date anymore and you basically wasted your time or designers can unintentionally introduce problems that were fixed previously. Keep track of the design versions and the problems that you have found in previous iterations (e.g. using screenshots). Before you test a new version of the design, check if you can find any usability problems that were already identified.

---
