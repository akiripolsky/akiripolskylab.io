# Default work allocation

## Purpose
Purpose of this document is to help me correctly set the expectations about
the amount of work I allocate for each of the groups I work with.

## Principles

- https://about.gitlab.com/handbook/engineering/ux/ux-research/#how-we-spend-our-time
- By default I allocate the same amount of time to each of the groups I work with.
- Rounding down.

## Allocation Option 1 - Group based

(If quarter has ~50 days.)

- Configure 11%
  - Solution Validation <10%: 1% (~0.5 day)
  - Problem Validation ~60%: 6% (~3 days)
- Monitor 11%
  - Solution Validation <10%: 1%
  - Problem Validation ~60%: 6%
- Distribution 11%
  - Solution Validation <10%: 1%
  - Problem Validation ~60%: 6%
- Geo 11%
  - Solution Validation <10%: 1%
  - Problem Validation ~60%: 6%
- Global search 11%
  - Solution Validation <10%: 1%
  - Problem Validation ~60%: 6%
- Infrastructure 11%
  - Solution Validation <10% : 1%
  - Problem Validation ~60%: 6%
- Memory 0%
- Database 0%
- Foundational/Strategic Research: ~30%

## Allocation Option 2 - Stage based

(If quarter has ~50 days.)

- Configure & Monitor 23% (2 groups)
  - Type of research
    - Solution Validation <10%: 2% (1 day)
    - Problem Validation ~60%: 13% (6.5 days)
- Enablement 46% (4 groups)
  - Type of research
    - Solution Validation <10%: 4% (2 days)
    - Problem Validation ~60%: 27% (13.5 days)
- Foundational/Strategic Research: ~30% (15 days)

## Time left

Other [responsibilities](https://about.gitlab.com/job-families/engineering/ux-researcher/#responsibilities-2) and duties:

- Deeply understand the technology and features of the stage groups to which you are assigned.
- Maintain a thorough knowledge of the direction and vision for your assigned stage groups. Consistently consider how your research can support the goals of your assigned stage groups, the broader product vision, and company objectives.
- Actively contribute to UX Research processes and documentation.
- Research coordination help
