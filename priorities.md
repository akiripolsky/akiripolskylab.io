# Priorities
<!-- TEMPLATE

### Week __
1.
1.
1.

* Product improvements: 
* Handbook improvements:

-->

---

This page is not updated anymore.

---

## Current week
### Week 27
1. [Improve UX Research Focus on Adoption Journeys](https://gitlab.com/gitlab-com/Product/-/issues/1175) / [CI Adoption Journey Research](https://gitlab.com/gitlab-org/ux-research/-/issues/922)
     1. Clarify the proposal
     1. Kick-off
 1. [Interviews about triage workflow **with Opsgenie**](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/31)
      1. Setup recruiting via FirstLook
1. [User testing of triage workflow](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/32)
    1. Take notes during the sessions
1. [Interview about Kubernetes cluster management](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/27)
     1. Take notes during the sessions
1. [Survey about requirements for monitoring](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/28)
    1. Create a survey in Qualtrics
    
* Product improvements: -
* Handbook improvements: -



## Past weeks
### Week 26
1. [Improve UX Research Focus on Adoption Journeys](https://gitlab.com/gitlab-com/Product/-/issues/1175)
     1. Discuss the proposal with my manager
1. [Interview about Kubernetes cluster management](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/27)
     1. Schedule participants
1. [User testing of triage workflow](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/32)
    1. Review scenario and update discussion guide
    1. Review pilot
 1. [Interviews about triage workflow **with Opsgenie**](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/31)
      1. Setup recruiting via Respondent.io
1. [Terraform module registry](https://gitlab.com/gitlab-org/gitlab/-/issues/216991#note_365678716) – Comments

* Product improvements: -
* Handbook improvements: -

### Week 25
1. [Interviews about triage workflow **with Opsgenie**](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/31)
     1. Recruit participants
1. [Survey about requirements for monitoring](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/28) 
     1. Finalize features descriptions
1. [Interview about Kubernetes cluster management](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/27)
     1. Recruit participants
1. [User testing of triage workflow](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/32)
     1. Create documentation

* Product improvements: –
* Handbook improvements: [Labels](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/53332)

### Week 24
1. [Survey about requirements for monitoring](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/28) 
     1. Draft features descriptions
     1. Try recruitment using data warehouse
1. [Interviews about triage workflow **with Opsgenie**](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/31)
     1. Kick-off
     1. Create documentation
1. Watch JTBD workshops

* Product improvements: -
* Handbook improvements: -
     
*Short week – Family and friends day*

### Week 23
1. [Interview about Kubernetes cluster management](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/27)
     1. Create research plan
     1. Create discussion guide
     1. Create screener
1. [Interviews about triage workflow](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/19)
     1. Document dogfooding and lessons learned
 1. [Survey about requirements for monitoring](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/28) 
      1. Review survey draft
      1. Try recruitment using data warehouse

* Product improvements: [Activity history bug](https://gitlab.com/gitlab-org/gitlab/-/issues/220399)
* Handbook improvements: [Checklists](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/52073), [Labels](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/30), [Recruiting template](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/29)
      

### Week 22
1. [Interview about Kubernetes cluster management](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/27)
     1. Intro to the topic
     1. Review related open research requests
1. [Interviews about triage workflow](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/19)
     1. Update insights
     1. Present the findings to the engineering teams
1. [Survey about requirements for monitoring](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/28) 
     1. Setup project
     
*Short week – Someone broken into my cellar so I spent whole Friday talking to police*        

### Week 21
1. [Interviews about triage workflow](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/19)
     1. Create insights
     1. Present the findings to the PM and Designer
1. [Interview about Kubernetes cluster management](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/27)
     1. Kick-off
     2. Setup project

### Week 20
1. [Interviews about triage workflow](https://gitlab.com/gitlab-org/ux-research/-/merge_requests/19)
     1. Learn to work with Dovetail, the new tool to document research work
     1. Setup project
     1. Create highlights from transcripts for all four participants

<!--
And also other: 
- IA thing
- CMS for Configure
-->

### Week 19
1. [Recruitment for the Configure Design Sprint](https://gitlab.com/gitlab-org/ux-research/-/issues/841)
1. Meeting the Package team
1. Q1 notes cleanup

*Short week – Family and friends day and Victory day*
